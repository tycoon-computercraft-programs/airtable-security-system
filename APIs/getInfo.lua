gIVersion = 1.5

local programName = "Get Info API"

local tempTableVariableThingyThatIDontCareAbout = fs.open("pSettings.txt", "r").readAll()
local pSettings = textutils.unserialize(tempTableVariableThingyThatIDontCareAbout)
local apiKey = pSettings["API Key"]
local baseURL = pSettings["Base URL"]

local sides = {
	["Front"] = "front",
	["Back"] = "back",
	["Left"] = "left",
	["Right"] = "right",
	["Top"] = "top",
	["Bottom"] = "bottom"
}

function getUserList()
    users = {}
    os.loadAPI("json")
    local temp = http.get(baseURL.."Users?maxRecords=100&view=Main%20View&api_key="..apiKey).readAll()
    local obj = json.decode(temp)

    for key, value in pairs(obj["records"]) do
		users[value["fields"]["Username"]] = {}
		for k, v in pairs(value["fields"]) do
			tempKey = k
			tempValue = v
			tempUser = value["fields"]["Username"]
			if k == "Username" or k == "UUID" then
				users[tempUser][tempKey] = tempValue
			end
		end
    end
    
    return users
end

function getUserInfo(userId)
	os.loadAPI("json")
	
	
	
	local temp = http.get("https://api.airtable.com/v0/appjdy9gM1GWO5b6f/Users/"..userId.."?api_key="..apiKey).readAll()
	local obj = json.decode(temp)
	
	local tempTable = {}
	
	for key, value in pairs(obj["fields"]) do
		tempTable[key] = value
	end

	return tempTable
end

function getDoorInfo(doorId)
    os.loadAPI("json")
	
	local asdf = http.get("https://api.airtable.com/v0/appjdy9gM1GWO5b6f/Door%20Settings/recfRchf2Pc7Rzwa8?api_key="..apiKey).readAll()
	local defaultSettings = json.decode(asdf)
	
    local temp = http.get("https://api.airtable.com/v0/appjdy9gM1GWO5b6f/Door%20Settings/"..doorId.."?api_key="..apiKey).readAll()
    local obj = json.decode(temp)
	
	
	tempTable = {}
	
	for key, value in pairs(defaultSettings["fields"]) do
		if key ~= "Door Name" then
			for k, v in pairs(sides) do if value == k then value = sides[k] end end
			tempTable[key] = value
		end
	end
	
	for key, value in pairs(obj["fields"]) do
		for k, v in pairs(sides) do if value == k then value = sides[k] end end
		tempTable[key] = value
	end
    
    return tempTable
end

function getTerminalInfo(terminalId)
    os.loadAPI("json")
	
	local asdf = http.get("https://api.airtable.com/v0/appjdy9gM1GWO5b6f/General%20Terminals/rec4pgOZZAAG5uYTn?api_key="..apiKey).readAll()
	local defaultSettings = json.decode(asdf)
	
    local temp = http.get("https://api.airtable.com/v0/appjdy9gM1GWO5b6f/General%20Terminals/"..terminalId.."?api_key="..apiKey).readAll()
    local obj = json.decode(temp)
	
	
	tempTable = {}
	
	for key, value in pairs(defaultSettings["fields"]) do
		if key ~= "Terminal Name" then
			for k, v in pairs(sides) do if value == k then value = sides[k] end end
			tempTable[key] = value
		end
	end
	
	for key, value in pairs(obj["fields"]) do
		for k, v in pairs(sides) do if value == k then value = sides[k] end end
		tempTable[key] = value
	end
    
    return tempTable
end

function getSysInfo(systemId)
	os.loadAPI("json")
	
	
	
	local temp = http.get("https://api.airtable.com/v0/appjdy9gM1GWO5b6f/General%20Defenses/"..systemId.."?api_key="..apiKey).readAll()
	local obj = json.decode(temp)
	
	local tempTable = {}
	
	for key, value in pairs(obj["fields"]) do
		tempTable[key] = value
	end

	for key, value in pairs(obj["fields"]) do
		for k, v in pairs(sides) do if value == k then value = sides[k] end end
		tempTable[key] = value
	end
    

	return tempTable
end

function getUserListOLD()
    users = {}
    os.loadAPI("json")
    local temp = http.get("https://api.airtable.com/v0/appjdy9gM1GWO5b6f/Users?maxRecords=100&view=Main%20View&api_key="..apiKey).readAll()
    local obj = json.decode(temp)

    for key, value in pairs(obj["records"]) do
		users[value["fields"]["Username"]] = {}
		for k, v in pairs(value["fields"]) do
			tempKey = k
			tempValue = v
			tempUser = value["fields"]["Username"]
			if k ~= "Username" then
				users[tempUser][tempKey] = tempValue
			end
		end
    end
    
    return users
end