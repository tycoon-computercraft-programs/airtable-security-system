os.loadAPI("getInfo.lua")

--os.pullEvent = os.pullEventRaw

local pSettings = textutils.unserialize(fs.open("pSettings.json", "r").readAll())
local sysId = pSettings["Identifier"]

local function printStatus(message, color)
	term.clear()
	term.setCursorPos(1,1)
	term.setBackgroundColor(colors.black)
	term.setBackgroundColor(colors.cyan)
	print(settings["System Name"].." | Type: "..settings["Type"].." Defense System"                           )
	term.setBackgroundColor(colors.black)
	print(" ")
	term.write("Status: ")
	term.setTextColor(color)
	term.write(message)
	term.setTextColor(colors.white)
	print(" ")
end

while true do
	printStatus("Getting Settings", colors.orange)
	settings = getInfo.getSysInfo(sysId)
	if settings["Active"] == true then
		printStatus("System Active", colors.green)
		rs.setOutput(settings["Output Side"], true)
	else
		printStatus("System Inactive", colors.red)
		rs.setOutput(settings["Output Side"], false)
	end
	sleep(math.random(3,7))
end