os.loadAPI("getInfo.lua")

local terminalId = "recGxZ0gPw6d4u3ne"

os.pullEvent = os.pullEventRaw

local function printStatus(message, color, accessLevel, adminReq)
	accessLevel = tostring(accessLevel)
	if accessLevel == "nil" then 
		accessLevel = "Access Level Not Found"
	elseif accessLevel == "0" then
		accessLevel = "Public Access"
	elseif accessLevel == "Checking Access Level" then
		accessLevel = "Checking Access Level"
	elseif tonumber(accessLevel) > 0 then
		accessLevel = "Access Level: "..tonumber(accessLevel)
	end
	if adminReq == true then
		accessLevel = "Administrator Required"
	end
	term.clear()
	term.setCursorPos(1,1)
	term.setBackgroundColor(colors.cyan)
	print("User Information Viewer | "..accessLevel.."                     ")
	term.setBackgroundColor(colors.black)
	print(" ")
	term.write("Status: ")
	term.setTextColor(color)
	term.write(message)
	term.setTextColor(colors.white)
	print(" ")
end

while true do
	printStatus("Getting Settings", colors.orange, "Checking Access Level", settings["Administrator Required"])
	local settings = getInfo.getTerminalInfo(terminalId)
	if settings["Active"] == nil then
		printStatus("Terminal Inactive", colors.red, settings["Access Level"], settings["Administrator Required"])
		while settings["Active"] == nil do
			settings = getInfo.getTerminalInfo(terminalId)
			sleep(math.random(3, 7))
		end
	end
	printStatus("Awaiting Keycard", colors.yellow, settings["Access Level"], settings["Administrator Required"])
	local event, side = os.pullEvent("disk")
	if event ~= "terminate" then
		settings = getInfo.getTerminalInfo(terminalId)
		if settings["Active"] == nil then
			disk.eject(side)
			printStatus("Terminal Inactive", colors.red, settings["Access Level"], settings["Administrator Required"])
			while settings["Active"] == nil do
				settings = getInfo.getTerminalInfo(terminalId)
				sleep(math.random(3, 7))
			end
			if settings["Active"] ~= nil then os.reboot() end
		end
		printStatus("Reading Keycard", colors.orange, settings["Access Level"], settings["Administrator Required"])
		local authData = textutils.unserialize(fs.open("disk/user", "r").readAll())
		local userData = getInfo.getUserInfo(authData["UUID"])
		if settings["Administrator Required"] == true then
			if userData["Administrator"] == nil then
				disk.eject(side)
				printStatus("Access Denied", colors.red, settings["Access Level"], settings["Administrator Required"])
				sleep(1)
				printStatus("Administrator Access Required", colors.red, settings["Access Level"], settings["Administrator Required"])
				sleep(2)
				printStatus("Rebooting", colors.red, settings["Access Level"], settings["Administrator Required"])
				sleep(1)
				os.reboot()
			end
		end
		if settings["Secondary Password Required"] == true then
			printStatus("Awaiting Secondary Password", colors.orange, settings["Access Level"], settings["Administrator Required"])
			print(" ")
			term.write("Secondary Password: ")
			local input = read("*")
			if input ~= userData["Secondary Password"] then
				disk.eject(side)
				printStatus("Secondary Password Invalid", colors.red, settings["Access Level"], settings["Administrator Required"])
				sleep(2)
				printStatus("Rebooting", colors.red, settings["Access Level"], settings["Administrator Required"])
				sleep(1)
				os.reboot()
			end
		end
		
		disk.eject(side)
		printStatus("Displaying User Info", colors.green, settings["Access Level"], settings["Administrator Required"])
		print(" ")
		print("Viewing User: "..authData["Username"])
		print(" ")
		print("Username: "..userData["Username"])
		term.write("User ID: ")
		if userData["Active"] == nil then
			term.setTextColor(colors.red)
			term.write("Inactive")
			term.setTextColor(colors.white)
		elseif userData["Active"] == true then
			term.setTextColor(colors.green)
			term.write("Active")
			term.setTextColor(colors.white)
		end
		print(" ")
		print("Access Level: "..userData["Access Level"])
		term.write("Administrator Privileges: ")
		if userData["Administrator"] == nil then
			term.setTextColor(colors.red)
			term.write("No")
			term.setTextColor(colors.white)
		elseif userData["Administrator"] == true then
			term.setTextColor(colors.green)
			term.write("Yes")
			term.setTextColor(colors.white)
		end
		print(" ")
		print(" ")
		print("Contact Database Administrator for Secondary Password")
		print(" ")
		print("Press Any Key to Continute")
		local event, keyNum = os.pullEvent("key")
		
		
	end
end