local programName = "Keycard Reader"
local version = 1.61

os.loadAPI("getInfo.lua")

--os.pullEvent = os.pullEventRaw

if fs.exists("prevSettings.txt") then
	local f = fs.open("prevSettings.txt", "r").readAll()
	local prevSettings = textutils.unserialize(f)
	rs.setOutput(prevSettings["Output Side"], true)
end

local pVersions = textutils.unserialize(http.get("https://gitlab.com/tycoon-computercraft-programs/airtable-security-system/raw/master/Miscellaneous/programVersions.txt").readAll())

local restart = false

if pVersions[programName] ~= version then
	print("Updating Main Program")
	local links = textutils.unserialize(http.get("https://gitlab.com/tycoon-computercraft-programs/airtable-security-system/raw/master/Miscellaneous/links.txt").readAll())
	local program = http.get(links[programName]).readAll()
	local h = fs.open("main.lua", "w")
	h.write(program)
	h.close()
	restart = true
end

if getInfo.gIVersion ~= pVersions["Get Info API"] then
	print("Updating Get Info API")
	local program = http.get("https://gitlab.com/tycoon-computercraft-programs/airtable-security-system/raw/master/APIs/getInfo.lua").readAll()
	local h = fs.open("getInfo.lua", "w")
	h.write(program)
	h.close()
	restart = true
end

if restart == true then
	os.reboot()
end

local pSettings = textutils.unserialize(fs.open("pSettings.txt", "r").readAll())
local doorId = pSettings["Identifier"]

local function accessGranted(outputSide, openTime)
	rs.setOutput(outputSide, false)
	sleep(openTime)
	rs.setOutput(outputSide, true)
end

local function printStatus(message, color, doorStatus, doorColor)
	local settings = getInfo.getDoorInfo(doorId)
	term.clear()
	term.setCursorPos(1,1)
	term.setBackgroundColor(colors.black)
	term.setBackgroundColor(colors.cyan)
	print(settings["Door Name"].." | Required Access Level: "..settings["Access Level"].."               ")
	term.setBackgroundColor(colors.black)
	print(" ")
	term.write("Status: ")
	term.setTextColor(color)
	term.write(message)
	term.setTextColor(colors.white)
	print(" ")
	term.write("Door Status: ")
	term.setTextColor(doorColor)
	term.write(doorStatus)
	term.setTextColor(colors.white)
end

while true do
	printStatus("Getting Settings", colors.orange, "Closed", colors.red)
	local settings = getInfo.getDoorInfo(doorId)
	rs.setOutput(settings["Output Side"], true)
	local prevS = fs.open("prevSettings.txt", "w")
	prevS.write(textutils.serialize(settings))
	prevS.close()
	--print(textutils.serialize(settings))
	if settings["Locked"] == true then
		printStatus("Locked", colors.red, "Closed", colors.red)
		while settings["Locked"] == true do
			settings = getInfo.getDoorInfo(doorId)
			sleep(math.random(3, 5))
		end
	end
	printStatus("Awaiting Keycard", colors.yellow, "Closed", colors.red)
	local event, p1 = os.pullEvent("disk")
	if event ~= "terminate" then
		settings = getInfo.getDoorInfo(doorId)
		if settings["Locked"] == true then
			disk.eject(p1)
			printStatus("Locked", colors.red, "Closed", colors.red)
			while settings["Locked"] == true do
				settings = getInfo.getDoorInfo(doorId)
				sleep(math.random(3, 5))
			end
			os.reboot()
		end
		if fs.exists("disk/user") == false then
			printStatus("Access Denied: Invalid Keycard", colors.red, "Closed", colors.red)
			disk.eject(p1)
			sleep(2)
			os.reboot()
		end
		local authData = textutils.unserialize(fs.open("disk/user", "r").readAll())
		local userData = getInfo.getUserInfo(authData["UUID"])
		if userData["Active"] == nil then
			printStatus("Access Denied: User ID Inactive", colors.red, "Closed", colors.red)
			disk.eject(p1)
			sleep(2)
		else
			if userData["Access Level"] >= settings["Access Level"] then
				if settings["Secondary Password Required"] == true then
					printStatus("Awaiting Secondary Password", colors.orange, "Closed", colors.red)
					print(" ")
					print(" ")
					term.write("Input Secondary Password: ")
					local input = read("*")
					if input == userData["Secondary Password"] then
						printStatus("Access Granted", colors.green, "Opened", colors.green)
						disk.eject(p1)
						accessGranted(settings["Output Side"], settings["Open Time"])
					else
						printStatus("Access Denied: Invlalid Secondary Password", colors.red, "Closed", colors.red)
						disk.eject(p1)
						sleep(2)
					end
				else
					disk.eject(p1)
					printStatus("Access Granted", colors.green, "Opened", colors.green)
					accessGranted(settings["Output Side"], settings["Open Time"])
				end
			else 
				printStatus("Access Denied: Access Level Insufficient", colors.red, "Closed", colors.red)
				print(" ")
				print(" ")
				print("Required Access Level: "..settings["Access Level"])
				print("Your Access Level: "..userData["Access Level"])
				disk.eject(p1)
				sleep(2)
			end
			disk.eject(p1)
		end
	end
end
