local h = fs.open("startup.lua", "w")
h.write('shell.run("installer.lua")')
h.close()

local links = textutils.unserialize(http.get("https://gitlab.com/tycoon-computercraft-programs/airtable-security-system/raw/master/Miscellaneous/links.txt").readAll())

local function header()
	term.clear()
	term.setCursorPos(1,1)
	term.setBackgroundColor(colors.cyan)
	print("Airtable Security System Auto Installer | Ver: 1.2     ")
    term.setBackgroundColor(colors.black)
    print(" ")
    print(" ")
end

local settings = {}

local function setType()
	sTypes = textutils.unserialize(http.get("https://gitlab.com/tycoon-computercraft-programs/airtable-security-system/raw/master/Miscellaneous/systemTypes.txt").readAll())
	local complete = false
	local tempTable = {}
	repeat
		header()
		print("Available System Types:")
		print(" ")
		for i = 1, #sTypes do
			textutils.pagedPrint("["..i.."] > "..sTypes[i])
			tempTable[sTypes[i]] = sTypes[i]
		end
		print(" ")
		print("Name or Number Accepted")
		print(" ")
		term.write("System Type: ")
		local input = read()
		if tempTable[input] == nil and sTypes[tonumber(input)] == nil then
			printError("Invalid Option")
			sleep(1)
		else
			tempValue = tonumber(input)
			if tempValue ~= nil then
				settings["System Type"] = sTypes[tempValue]
			else
				settings["System Type"] = input
			end
			complete = true
		end
	until complete == true
end

local function setIdentifier()
	header()
	term.write("System Identifier: ")
	settings["System Identifier"] = read()
end

local function setAPIKey()
	header()
	term.write("Airtable API Key: ")
	settings["API Key"] = read()
end

local function setBaseURL()
	header()
	term.write("Airtable Base URL: ")
	settings["Base URL"] = read()
end

local function correctSettings()
	header()
	local ready = false
	repeat

		for key, value in pairs(settings) do
			print(key..": "..value)
		end

		term.write("Setting to Correct: ")
		local input = read()

		if input == "System Type" then
			setType()
			if settings["System Type"] ~= "Keycard Maker" then
				setIdentifier()
			end
		elseif input == "System Identifier" then
			setIdentifier()
		elseif input == "API Key" then
			setAPIKey()
		elseif input == "Base URL" then
			setBaseURL()
		else
			print("Invalid Option")
			sleep(1)
		end

		header()
		for key, value in pairs(settings) do
			print(key..": "..value)
		end
		term.write("Correct Additional Settings (Y/n): ")
		local input = read()
		if input == "n" then
			ready = true
		end
	until ready == true
end

setType()
setAPIKey()
setBaseURL()

if settings["System Type"] ~= "Keycard Maker" then
	setIdentifier()
end

local correct = false
repeat
	header()
	print("Current Settings: ")
	print(" ")
	for key, value in pairs(settings) do
		print(key..": "..value)
	end
	print(" ")
	term.write("Is this correct (Y/n): ")
	local input = read()
	if input == "Y" then
		correct = true
	elseif input == "n" then
		correct = false
		print("Incorrect Settings")
		correctSettings()
	else
		correct = false
	end
	sleep(1)
until correct == true

header()
print(" ")
print("Saving Settings...")
local s = fs.open("pSettings.txt", "w")
s.write(textutils.serialize(settings))
s.close()

header()
print("Getting JSON API")
local h = http.get(links["json API"]).readAll()
local j = fs.open("json", "w")
j.write(h)
j.close()

header()
print("Getting getInfo API")
local h = http.get(links["getInfo API"]).readAll()
local g = fs.open("getInfo.lua", "w")
g.write(h)
g.close()

header()
print("Getting "..settings["System Type"].." Program")
local h = http.get(links[settings["System Type"]]).readAll()
local m = fs.open("main.lua", "w")
m.write(h)
m.close()

header()
print("Writing Startup File")
local f = fs.open("startup.lua", "w")
f.write('shell.run("main.lua")')
f.close()

header()
print("Uninstalling Installer")
--fs.delete("installer.lua")

header()
print("Restarting")
sleep(1)
os.reboot()