local programName = "Keycard Maker"
local version = 1.6

os.loadAPI("getInfo.lua")

--os.pullEvent = os.pullEventRaw

local h = http.get("https://gitlab.com/tycoon-computercraft-programs/airtable-security-system/raw/master/Miscellaneous/programVersions.txt")
local pVersions = textutils.unserialize(h.readAll())

local restart = false

if pVersions[programName] ~= version then
	local links = textutils.unserialize(http.get("https://gitlab.com/tycoon-computercraft-programs/airtable-security-system/raw/master/Miscellaneous/links.txt").readAll())
	local program = http.get(links[programName]).readAll()
	local h = fs.open("main.lua", "w")
	h.write(program)
	h.close()
	restart = true
end

if getInfo.gIVersion ~= pVersions["Get Info API"] then
	local program = http.get("https://gitlab.com/tycoon-computercraft-programs/airtable-security-system/raw/master/APIs/getInfo.lua").readAll()
	local h = fs.open("getInfo.lua", "w")
	h.write(program)
	h.close()
	restart = true
end

if restart == true then
	os.reboot()
end

local function printStatus(message, color, currentUser, userColor)
	term.clear()
	term.setCursorPos(1,1)
	term.setBackgroundColor(colors.cyan)
	print("Keycard Maker Ver: "..version.." | Administrator Access Required       ")
	term.setBackgroundColor(colors.black)
	print(" ")
	term.write("Status: ")
	term.setTextColor(color)
	term.write(message)
	term.setTextColor(colors.white)
	print(" ")
	term.write("Current User: ")
	term.setTextColor(userColor)
	term.write(currentUser)
	term.setTextColor(colors.white)
	print(" ")
end


while true do
    printStatus("Awaiting Administrator Keycard", colors.orange, "Awaiting Authentication", colors.orange)
	local event, side = os.pullEvent("disk")
	if event ~= "terminate" then
		printStatus("Reading Keycard. Please Wait.", colors.yellow, "Authenticating User", colors.orange)
		if fs.exists("disk/user") == false then
			disk.eject(side)
			printStatus("Invalid Keycard", colors.red, "Authentication Failed", colors.red)
			sleep(2)
			os.reboot()
		end
		local authData = textutils.unserialize(fs.open("disk/user", "r").readAll())
		local userData = getInfo.getUserInfo(authData["UUID"])
		if userData["Active"] == nil then
			disk.eject(side)
			printStatus("User ID Inactive", colors.red, "Authentication Failed", colors.red)
			sleep(2)
			printStatus("Rebooting", colors.red, "Authentication Failed", colors.red)
			sleep(1.5)
			os.reboot()
		end
		if userData["Administrator"] ~= nil then
			printStatus("Awaiting Second Factor", colors.orange, "Authentication In Progress", colors.orange)
			print(" ")
			term.write("User Password: ")
			local input = read("*")
			if input ~= userData["Secondary Password"] then
				disk.eject(side)
				printStatus("Invalid Second Factor", colors.orange, "Authentication Failed", colors.red)
				sleep(2)
				printStatus("Rebooting", colors.red, "Authentication Failed", colors.red)
			else
				printStatus("User Authenticated", colors.green, userData["Username"], colors.green)
				disk.eject(side)
				sleep(1)
				printStatus("Awaiting Blank Keycard", colors.orange, userData["Username"], colors.green)
				print(" ")
				print("Insert Blank Keycard")
				local event, side = os.pullEvent("disk")
				if event ~= "terminate" then
					printStatus("Awaiting User Input", colors.orange, userData["Username"], colors.green)
					print(" ")
					local users = getInfo.getUserList()
					print("Available Users: ")
					for key, value in pairs(users) do
						print("> "..key)
					end
					print(" ")
					term.write("Enter Username: ")
					local input = read()
					if users[input] == nil then
						disk.eject(side)
						printStatus("Invalid Username", colors.red, userData["Username"], colors.green)
						print(" ")
						sleep(1.5)
						printStatus("Rebooting", colors.red, userData["Username"], colors.green)
						sleep(1.5)
						os.reboot()
					end
					printStatus("Writing Keycard: "..users[input]["Username"], colors.orange, userData["Username"], colors.green)
					sleep(1)
					local h = fs.open("disk/user", "w")
					h.write(textutils.serialize(users[input]))
					h.close()
					disk.setLabel(side, users[input]["Username"].."'s Keycard")
					printStatus("Keycard Written: "..users[input]["Username"], colors.green, userData["Username"], colors.green)
					disk.eject(side)
					sleep(1)
				end
			end
		else
			disk.eject(side)
			printStatus("Unauthorized User: Not an Administrator", colors.red, "Authentication Failed", colors.red)
			sleep(3)
		end
	end
end